import requests
from cons import const
import mysql.connector
from bs4 import BeautifulSoup
import json
import datetime
import traceback

__author__ = 'E425'

broker_id = {}

db = mysql.connector.connect(**const.ocean_mysql)
cursor = db.cursor(buffered=True)


def check_broker(broker_name):
    if broker_name not in broker_id:
        cursor.execute("INSERT INTO stockgrid.BROKER (N_short_name) VALUES (%(broker_name)s)",
                       {'broker_name': broker_name})
        broker_id[broker_name] = cursor.lastrowid

    return broker_id[broker_name]


if __name__ == '__main__':

    try:
        # fetch all broker id
        cursor.execute("select I_broker, N_short_name from stockgrid.BROKER")

        for _broker_id, _broker_name, in cursor:
            broker_id[_broker_name] = _broker_id

        # fetch all normal symbol
        cursor.execute("select symbol from stockgrid.datastock where id >= 0 order by symbol")

        is_continue = False

        for _symbol, in cursor.fetchall():
            print(_symbol)

            # if _symbol == 'ITD':
            #     is_continue = True
            #
            # if not is_continue:
            #     continue

            r = requests.get(
                'http://www.settrade.com/AnalystConsensus/C04_10_stock_saa_p1.jsp?txtSymbol={}&selectPage=10'.format(
                    _symbol))

            # change encoding
            r.encoding = 'tis-620'
            soup = BeautifulSoup(r.text)

            # find column name
            heading = soup.select('div[style*="background:#8C8C8C;"]')

            # have some research
            if heading:
                heading = heading[0]

                # them have 2 type row
                body1 = soup.find_all("div", class_="tablecolor1") + soup.find_all("div", class_="tablecolor2")

                heading_key = []

                for head_text in heading.text.split('\n'):
                    head_text = head_text.strip()
                    if head_text and head_text != 'EPS (Baht)':

                        # find year with no space
                        if head_text.find('201') > 0:
                            head_text = head_text.replace('201', ' 201')
                        elif head_text.find('201') == 0:
                            head_text = head_text.replace('201', 'EPS 201')

                        if head_text == '%Change':
                            heading_key.append(heading_key[-1] + ' EPS %Change')
                        else:
                            heading_key.append(head_text)
                else:
                    heading_key.append('research_link')

                for body in body1:
                    all_data_dict = {}
                    values = body.find_all(class_='fl')

                    for index, value in enumerate(values):
                        if not value.text:
                            all_data_dict[heading_key[index]] = 'http://www.settrade.com' + \
                                                                value.findChildren()[0].get('href')
                        elif value.text.strip() == '-':
                            all_data_dict[heading_key[index]] = None
                        else:
                            all_data_dict[heading_key[index]] = value.text.strip()

                    print(all_data_dict)
                    cursor.execute("""
                    REPLACE INTO stockgrid.DATA_BROKER_RESEARCH (
                        I_security,
                        I_broker,
                        D_update,
                        N_rec,
                        R_target_price,
                        N_forcast_data_json,
                        N_reseach_link )
                    VALUES (
                        (SELECT id FROM stockgrid.datastock WHERE symbol = %(symbol)s),
                        %(broker_id)s,
                        %(update_date)s,
                        %(rec)s,
                        %(target_price)s,
                        %(json_data)s,
                        %(research_link)s);
                    """, {
                        'symbol': _symbol,
                        'broker_id': check_broker(all_data_dict['Broker']),
                        'update_date': datetime.datetime.strptime(all_data_dict['LastUpdate'], '%d/%m/%y').date(),
                        'rec': all_data_dict['Rec'],
                        'target_price': all_data_dict['TargetPrice'],
                        'json_data': json.dumps(all_data_dict),
                        'research_link': all_data_dict['research_link'],
                    })
    except:
        print(traceback.format_exc())