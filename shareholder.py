from cons import const
import datetime
import json
import mysql.connector
import pyodbc
import collections
import re
import operator

__author__ = 'E425'


def ice_float(number):
    if not number is None:
        return float(number.replace(',', ''))
    return number

def is_24_or_not(list):
    if len(list) > 100:
        return 100;
    return len(list)

def top_net_worth(): #Everything
    show_json = []
    sorted_x = sorted(temp_name_value.items(), key=operator.itemgetter(1), reverse=True)
    for num in range(0, is_24_or_not(sorted_x)):
        #print('Net %d:%s:%f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        show_json.append([sorted_x[num][0], sorted_x[num][1]])
    json.dump(show_json, open('top_net_worth.json', 'w'))


def top_individual(): #normal people
    show_json = []
    sorted_x = sorted(temp_individual.items(), key=operator.itemgetter(1), reverse=True)
    for num in range(0, is_24_or_not(sorted_x)):
        #print('Indi %d:%s:%f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        show_json.append([sorted_x[num][0], sorted_x[num][1]])
    json.dump(show_json, open('top_individual.json', 'w'))


def top_institution(): #company
    show_json = []
    sorted_x = sorted(temp_institution.items(), key=operator.itemgetter(1), reverse=True)
    for num in range(0, is_24_or_not(sorted_x)):
        #print('Insti %d:%s:%f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        show_json.append([sorted_x[num][0], sorted_x[num][1]])
    json.dump(show_json, open('top_institution.json', 'w'))

def top_foreign(): #other
    show_json = []
    sorted_x = sorted(temp_foreign.items(), key=operator.itemgetter(1), reverse=True)
    for num in range(0, is_24_or_not(sorted_x)):
        #print('For %d:%s:%f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        show_json.append([sorted_x[num][0], sorted_x[num][1]])
    json.dump(show_json, open('top_foreign.json', 'w'))

def top_gainer():
    show_json = []
    sorted_x = sorted(temp_lastday.items(), key=operator.itemgetter(1), reverse=True)
    for num in range(0, is_24_or_not(sorted_x)):
        #print('Gainer %d:%s:%f:%f' % (num ,sorted_x[num][0], sorted_x[num][1][0], sorted_x[num][1][1]))
        show_json.append([sorted_x[num][0], sorted_x[num][1][0], sorted_x[num][1][1]])
    json.dump(show_json, open('top_gainer.json', 'w'))

def top_loser():
    show_json = []
    sorted_x = sorted(temp_lastday.items(), key=operator.itemgetter(1))
    for num in range(0, is_24_or_not(sorted_x)):
        #print('Loser %d:%s:%f:%f' % (num ,sorted_x[num][0], sorted_x[num][1][0], sorted_x[num][1][1]))
        show_json.append([sorted_x[num][0], sorted_x[num][1][0], sorted_x[num][1][1]])
    json.dump(show_json, open('top_loser.json', 'w'))



def main():
    pass


if __name__ == '__main__':


    mysql_db = mysql.connector.connect(**const.ocean_mysql)
    sql_db = pyodbc.connect(const.windowsthweb_test)
    azure_db = pyodbc.connect(const.windowsthdata_test)

    mysql_cursor = mysql_db.cursor()
    sql_cursor = sql_db.cursor()
    azure_cursor = azure_db.cursor()

    mini_db = []
    mini_avg_db = []
    security_ids = set()

    name_ids = set()
    surname_ids = set()

    temp_name_value = {}
    temp_surname_value = {}

    temp_individual = {}
    temp_institution = {}
    temp_foreign = {}

    temp_date = {}

    temp_lastchange = {}
    temp_lastnet = {}
    temp_lastday = {}

    #now = datetime.datetime.now()
    #now = datetime.date(2013,9,26)
    #print(str(now))
    #yesterday = now - datetime.timedelta(days=1)
    #print(str(yesterday))

    sql_cursor.execute("""SELECT TOP 2 [oaq_date]
        FROM [stockgrid].[dateoaq]
        order by [oaq_date] desc""")

    for index, ( _date,) in enumerate(sql_cursor):
        temp_date[index] = _date

    sql_cursor.execute("SELECT sum(CONVERT(float, a.[Q_SHARE]) * c.[cclose]) AS value ,a.[N_TITLE_HLD],a.[N_FIRST_HLD],a.[N_LAST_HLD],a.I_HOLDER_TYPE "
              + " FROM [psims].[SHAREHOLDER] a inner join ( select max(D_BOOK_CLOSED) D_BOOK_CLOSED, I_SECURITY from [psims].[SHAREHOLDER] group by I_SECURITY) b "
              + " on a.I_SECURITY = b.I_SECURITY and a.D_BOOK_CLOSED = b.D_BOOK_CLOSED inner join (SELECT I_security, N_SECURITY FROM [psims].[SECURITY]) d "
              + "on a.I_SECURITY = d.I_SECURITY inner join (SELECT [symbol],[cclose] FROM [stockgrid].[history] where [datadate] = '"
              + temp_date[0] + "') c on c.symbol = d.N_SECURITY group by a.[N_TITLE_HLD],a.[N_FIRST_HLD],a.[N_LAST_HLD],a.I_HOLDER_TYPE "
			  + " order by value desc""")

    print("YYY")

    for index, ( _value, _title_name, _first_name, _last_name, _type) in enumerate(sql_cursor):
        name_ids.add(_title_name+','+_first_name+','+_last_name)
        temp_name_value[_title_name+','+_first_name+','+_last_name] = _value
        if(_type is '2' or _type is '3'):
            temp_foreign[_title_name+','+_first_name+','+_last_name] = _value
        elif(_type is '1'):
            temp_individual[_title_name+','+_first_name+','+_last_name] = _value
        else:
            temp_institution[_title_name+','+_first_name+','+_last_name] = _value


    sql_cursor.execute("SELECT sum(CONVERT(float, a.[Q_SHARE]) * c.[cclose]) AS value ,a.[N_TITLE_HLD],a.[N_FIRST_HLD],a.[N_LAST_HLD],a.I_HOLDER_TYPE "
              + " FROM [psims].[SHAREHOLDER] a inner join ( select max(D_BOOK_CLOSED) D_BOOK_CLOSED, I_SECURITY from [psims].[SHAREHOLDER] group by I_SECURITY) b "
              + " on a.I_SECURITY = b.I_SECURITY and a.D_BOOK_CLOSED = b.D_BOOK_CLOSED inner join (SELECT I_security, N_SECURITY FROM [psims].[SECURITY]) d "
              + "on a.I_SECURITY = d.I_SECURITY inner join (SELECT [symbol],[cclose] FROM [stockgrid].[history] where [datadate] = '"
              + temp_date[1] + "') c on c.symbol = d.N_SECURITY group by a.[N_TITLE_HLD],a.[N_FIRST_HLD],a.[N_LAST_HLD],a.I_HOLDER_TYPE "
			  + " order by value desc""")

    print("YYY")

    for index, ( _value, _title_name, _first_name, _last_name, _type) in enumerate(sql_cursor):
        #print("%s %f - %f = %f" % (_first_name+_last_name, temp_name_value[_title_name+','+_first_name+','+_last_name], _value, temp_name_value[_title_name+','+_first_name+','+_last_name] - _value))
        #temp_lastnet[_title_name+','+_first_name+','+_last_name] = temp_name_value[_title_name+','+_first_name+','+_last_name] - _value
        if(_value != 0):
            #temp_lastchange[_title_name+','+_first_name+','+_last_name] = ((temp_name_value[_title_name+','+_first_name+','+_last_name]/_value) - 1) * 100
            temp_lastday[_title_name+','+_first_name+','+_last_name] = [temp_name_value[_title_name+','+_first_name+','+_last_name] - _value, ((temp_name_value[_title_name+','+_first_name+','+_last_name]/_value) - 1) * 100]
            #print("value if %f" % _value)
        else:
            #temp_lastchange[_title_name+','+_first_name+','+_last_name] = (temp_name_value[_title_name+','+_first_name+','+_last_name])
            temp_lastday[_title_name+','+_first_name+','+_last_name] = [temp_name_value[_title_name+','+_first_name+','+_last_name] - _value, (temp_name_value[_title_name+','+_first_name+','+_last_name])]
            #print("value else %f" % _value)

    ####Surname
    #sql_cursor.execute("SELECT sum(CONVERT(float, a.[Q_SHARE]) * c.[cclose]) AS value ,a.[N_LAST_HLD] "
    #          + " FROM [psims].[SHAREHOLDER] a inner join ( select max(D_BOOK_CLOSED) D_BOOK_CLOSED, I_SECURITY from [psims].[SHAREHOLDER] group by I_SECURITY) b "
    #          + " on a.I_SECURITY = b.I_SECURITY and a.D_BOOK_CLOSED = b.D_BOOK_CLOSED inner join (SELECT I_security, N_SECURITY FROM [psims].[SECURITY]) d "
    #          + "on a.I_SECURITY = d.I_SECURITY inner join (SELECT [symbol],[cclose] FROM [stockgrid].[history] where [datadate] = '"
    #          + temp_date[0] + "') c on c.symbol = d.N_SECURITY group by a.[N_LAST_HLD] "
	#		  + " order by value desc""")

    #for index, ( _value, _last_name) in enumerate(sql_cursor):
    #    surname_ids.add(_last_name)
    #    temp_surname_value[_last_name] = _value





    top_net_worth()
    top_individual()
    top_institution()
    top_foreign()
    top_gainer()
    top_loser()


    print("Finish Programme")