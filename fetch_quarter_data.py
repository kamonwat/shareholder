from collections import defaultdict
from cons import const, mail
import datetime
import os
import traceback
import requests
import mysql.connector
from bs4 import BeautifulSoup


__author__ = 'E425'

all_data = defaultdict(list)


db = mysql.connector.connect(**const.ocean_mysql)
cursor = db.cursor(buffered=True)


if __name__ == '__main__':
    try:
        for market in ['SET', 'mai']:
            r = requests.get('http://www.settrade.com/AnalystConsensus/CompareData.jsp?compareTopic=10&market=' + market)

            # change encoding
            r.encoding = 'tis-620'

            if not os.path.exists('log'):
                os.mkdir('log')

            with open('log/{market} {date}.log'.format(market=market, date=datetime.date.today()), 'w', encoding='utf-8') as f:
                f.write(r.text)

            soup = BeautifulSoup(r.text)

            trs = soup.select('thead')[0]

            for tr in trs.nextSiblingGenerator():
                if tr.name == 'tr':
                    if tr.text.find(':') >= 0:
                        continue

                    tds = tr.find_all('td')

                    symbol = None
                    for index, td in enumerate(tds):
                        if index:
                            all_data[symbol].append(td.text.strip().replace(',', '') if td.text.strip() != '-' else None)
                        else:
                            symbol = td.text.strip()

        for symbol, insert_value in all_data.items():
            print(symbol, insert_value[11])
            cursor.execute("""
                REPLACE INTO stockgrid.DATA_BROKER_AVG_FORECAST (
                    I_security,
                    R_actual_net_profit,
                    R_quarter_1_this_year,
                    R_quarter_2_this_year,
                    R_quarter_3_this_year,
                    R_quarter_4_this_year,
                    R_quarter_1_next_year,
                    R_quarter_2_next_year,
                    R_quarter_3_next_year,
                    R_quarter_4_next_year,
                    R_pe_forecast,
                    R_pbv_forecast,
                    R_div_forecast,
                    R_target_price)
                VALUES (
                    (SELECT id FROM stockgrid.datastock WHERE symbol = %s),
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s,
                    %s);
                """, [symbol] + insert_value)
    except:
        e = traceback.format_exc()
        print(e)
        mail.send(e, '[SET] quarter forecast data')