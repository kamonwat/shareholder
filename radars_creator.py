#!/usr/bin/env python
# -*- coding: utf-8 -*-
from collections import defaultdict
from cons import const, mail
import decimal
from datetime import date, datetime
import traceback
from bs4 import BeautifulSoup
import mysql.connector
import json
from azure.storage import BlobService
import pyodbc
import requests


blobService = BlobService(*const.blob_stockradars)

connect__local_db = mysql.connector.connect(**const.ocean_mysql)
cursor_local = connect__local_db.cursor()

cursor_local.execute("SET NAMES utf8mb4;")  # or utf8 or any other charset you want to handle

cursor_local.execute("SET CHARACTER SET utf8mb4;")  # same as above

cursor_local.execute("SET character_set_connection=utf8mb4;")  # same as above


def complex_python(obj):
    if isinstance(obj, decimal.Decimal):
        return float(obj)
    elif isinstance(obj, datetime):
        return obj.strftime('%Y-%m-%d')
    elif isinstance(obj, date):
        return obj.strftime('%Y-%m-%d')
    raise TypeError


def gen_cash_balance_lists():
    cursor_local.execute("""SELECT
                                A.`symbol`, A.`start_date`, A.`end_date`
                            FROM
                                stockgrid.calendar A,
                                stockgrid.datastock B
                            WHERE
                                `event_type` = 'cash balance'  and
                                `start_date` <= sysdate() and
                                `end_date` >= sysdate() and
                                A.`symbol` = B.`symbol`
                            order by
                                start_date desc,
                                B.marketcap desc;""")

    cash_balance_lists = []
    json_table = []

    for symbol, start_date, end_date, in cursor_local.fetchall():
        cash_balance_lists.append(symbol)
        json_table.append({
            'Name': symbol,
            'Start Date': start_date,
            'End Date': end_date
        })

    cursor_local.execute("""SELECT
                                A.`symbol`
                            FROM
                                stockgrid.calendar A,
                                stockgrid.datastock B
                            WHERE
                                `event_type` = 'cash balance'  and
                                `end_date` = cast(sysdate() as Date) and
                                A.`symbol` = B.`symbol`
                            order by
                                start_date desc,
                                B.marketcap desc;""")

    gone_list = []

    for symbol, in cursor_local.fetchall():
        gone_list.append(symbol)

    upcoming_cash_balance_lists = []

    r = requests.post('http://www.set.or.th/set/marketalertnews.do',
                      data={'cashBalance': 'on',
                            'symbol': '',
                            'headline': '',
                            'from': date.today().strftime('%d/%m/%Y'),  # 28/11/2014
                            'to': date.today().strftime('%d/%m/%Y'),
                            'submit': '%A4%E9%B9%CB%D2',
                            'language': 'en',
                            'country': 'TH'})

    soup = BeautifulSoup(r.text)
    symbols = soup.find_all('td', nowrap='nowrap')

    for symbol in symbols:
        if symbol.text.strip() == 'Detail' or symbol.text.strip() not in ALL_SYMBOL:
            pass
        elif symbol.text.strip() in gone_list:
            cash_balance_lists.append(symbol.text.strip())
        else:
            upcoming_cash_balance_lists.append(symbol.text.strip())

    # gen_upcoming_cash_balance_lists

    cursor_local.execute("""SELECT
                                A.`symbol`, A.`start_date`, A.`end_date`
                            FROM
                                stockgrid.calendar A,
                                stockgrid.datastock B
                            WHERE
                                `event_type` = 'cash balance'  and
                                cast(`start_date` as DATE) > sysdate() and
                                A.`symbol` = B.`symbol`
                            order by
                                start_date desc,
                                B.marketcap desc;""")

    upcoming_json_table = []

    for symbol, start_date, end_date, in cursor_local.fetchall():
        if symbol in cash_balance_lists or symbol in upcoming_cash_balance_lists:
            continue

        upcoming_cash_balance_lists.append(symbol)
        upcoming_json_table.append({
            'Name': symbol,
            'Start Date': start_date,
            'End Date': end_date
        })

    blobService.put_block_blob_from_text('radars',
                                         'Cash Balance.json',
                                         json.dumps(cash_balance_lists),
                                         x_ms_blob_content_type='application/json')

    blobService.put_block_blob_from_text('radars-data',
                                         'Cash Balance Radars.json',
                                         json.dumps({
                                             'desc': u'Check Cash Balance stock list that is announced by SET every evening Friday, can be traded by cash only. (no credit account allowed)',
                                             'header': [],
                                             'data': [
                                                 {'name': 'Unrestricted',
                                                  'desc': u'รายชื่อหุ้นที่กำลังจะหลุด Cash Balance'},
                                                 {'name': 'Restricted',
                                                  'desc': u'รายชื่อหุ้นที่กำลังจะเข้า Cash Balance'},
                                                 {'name': 'Cash Balance',
                                                  'desc': u'หุ้นที่ต้องใช้เงินสดซื้อขายเท่านั้น (ใช้ Account ประเภท Cash Balance)'}]}),
                                         x_ms_blob_content_type='application/json')

    blobService.put_block_blob_from_text('radars-data',
                                         'Cash Balance.json',
                                         json.dumps({
                                                        'desc': u'หุ้นที่ต้องใช้เงินสดซื้อขายเท่านั้น (ใช้ Account ประเภท Cash Balance)',
                                                        'header': ['Name', 'Start Date', 'End Date'],
                                                        'data': json_table}, default=complex_python),
                                         x_ms_blob_content_type='application/json')

    blobService.put_block_blob_from_text('radars',
                                         'Restricted.json',
                                         json.dumps(upcoming_cash_balance_lists),
                                         x_ms_blob_content_type='application/json')

    blobService.put_block_blob_from_text('radars-data',
                                         'Restricted.json',
                                         json.dumps({'desc': u'รายชื่อหุ้นที่กำลังจะเข้า Cash Balance',
                                                     'header': ['Name', 'Start Date', 'End Date'],
                                                     'data': upcoming_json_table}, default=complex_python),
                                         x_ms_blob_content_type='application/json')
    # gen_gone_cash_balance_lists

    cursor_local.execute("""SELECT
                                A.`symbol`, A.`start_date`, A.`end_date`
                            FROM
                                stockgrid.calendar A,
                                stockgrid.datastock B
                            WHERE
                                `event_type` = 'cash balance'  and
                                `end_date` = cast(sysdate() as date) and
                                A.`symbol` = B.`symbol`
                            order by
                                start_date desc,
                                B.marketcap desc;""")

    gone_cash_balance_lists = []
    json_table = []

    for symbol, start_date, end_date, in cursor_local.fetchall():
        if symbol in cash_balance_lists or symbol in upcoming_cash_balance_lists:
            continue

        gone_cash_balance_lists.append(symbol)
        json_table.append({
            'Name': symbol,
            'Start Date': start_date,
            'End Date': end_date
        })

    radars_count['Cash Balance Radars'] = [
        {"count": len(cash_balance_lists), "name": "Cash Balance", "key": "Cash Balance"},
        {"count": len(gone_cash_balance_lists), "name": "Unrestricted", "key": "Unrestricted"},
        {"count": len(upcoming_cash_balance_lists), "name": "Restricted", "key": "Restricted"}]

    blobService.put_block_blob_from_text('radars',
                                         'Unrestricted.json',
                                         json.dumps(gone_cash_balance_lists),
                                         x_ms_blob_content_type='application/json')

    blobService.put_block_blob_from_text('radars-data',
                                         'Unrestricted.json',
                                         json.dumps({'desc': u'รายชื่อหุ้นที่กำลังจะหลุด Cash Balance',
                                                     'header': ['Name', 'Start Date', 'End Date'],
                                                     'data': json_table}, default=complex_python),
                                         x_ms_blob_content_type='application/json')


def gen_nvdr_top_24_buy():
    cursor_local.execute("""SELECT symbol, datadate, volume_buy, volume_sell, volume_total, volume_net, volume_percent, value_buy, value_sell, value_total, value_net, value_percent
                            FROM stockgrid.nvdr_history
                            WHERE
                                datadate =
                                    (SELECT datadate FROM stockgrid.nvdr_history ORDER BY datadate desc LIMIT 1) and
                                symbol in (select symbol from stockgrid.datastock)
                            ORDER BY value_buy desc LIMIT 24;
                         """)

    json_table = []
    nvdr_top_24_buy_lists = []

    for symbol, datadate, volume_buy, volume_sell, volume_total, volume_net, volume_percent, value_buy, value_sell, value_total, value_net, value_percent, in cursor_local.fetchall():
        nvdr_top_24_buy_lists.append(symbol)
        json_table.append({
            'Name': symbol,
            'Buy': value_buy
        })

    radars_count['NVDR'] = [{"count": len(nvdr_top_24_buy_lists), "name": "Top 24 NVDR Buy", "key": "Top 24 NVDR Buy"}]

    blobService.put_block_blob_from_text('radars',
                                         'Top 24 NVDR Buy.json',
                                         json.dumps(nvdr_top_24_buy_lists),
                                         x_ms_blob_content_type='application/json')

    blobService.put_block_blob_from_text('radars-data',
                                         'Top 24 NVDR Buy.json',
                                         json.dumps({'desc': u'หุ้น 24 ตัวแรกที่ NVDR ซื้อเยอะที่สุด',
                                                     'header': ['Name', 'Buy'],
                                                     'data': json_table}, default=complex_python),
                                         x_ms_blob_content_type='application/json')

    blobService.put_block_blob_from_text('radars-data',
                                         'NVDR.json',
                                         json.dumps({
                                             'desc': u'Check which stock interesting for foreign investors.',
                                             'header': [],
                                             'data': [
                                                 {'name': 'Top 24 NVDR Buy',
                                                  'desc': u'หุ้น 24 ตัวแรกที่ NVDR ซื้อเยอะที่สุด'},
                                                 {'name': 'Top 24 NVDR Sell',
                                                  'desc': u'หุ้น 24 ตัวแรกที่ NVDR ขายเยอะที่สุด'},
                                                 {'name': 'Top 24 NVDR Most Active',
                                                  'desc': u'หุ้น 24 ตัวแรกที่ NVDR มีมูลค่าซื้อขายเยอะที่สุด'},
                                                 {'name': 'Top 24 NVDR Ratio',
                                                  'desc': u'หุ้น 24 ตัวแรก ที่มีสัดส่วนนักลงทุนต่างชาติเข้ามาซื้อขายเยอะที่สุด โดยคิดจากมูลค่าซื้อขาย'}]}),
                                         x_ms_blob_content_type='application/json')


def gen_nvdr_top_24_sell():
    cursor_local.execute("""SELECT symbol, datadate, volume_buy, volume_sell, volume_total, volume_net, volume_percent, value_buy, value_sell, value_total, value_net, value_percent
                            FROM stockgrid.nvdr_history
                            WHERE
                                datadate =
                                    (SELECT datadate FROM stockgrid.nvdr_history ORDER BY datadate desc LIMIT 1) and
                                symbol in (select symbol from stockgrid.datastock)
                            ORDER BY value_sell desc LIMIT 24;
                         """)

    json_table = []
    nvdr_top_24_sell_lists = []

    for symbol, datadate, volume_buy, volume_sell, volume_total, volume_net, volume_percent, value_buy, value_sell, value_total, value_net, value_percent, in cursor_local.fetchall():
        nvdr_top_24_sell_lists.append(symbol)
        json_table.append({
            'Name': symbol,
            'Sell': value_sell
        })

    radars_count['NVDR'].append(
        {"count": len(nvdr_top_24_sell_lists), "name": "Top 24 NVDR Sell", "key": "Top 24 NVDR Sell"})
    blobService.put_block_blob_from_text('radars',
                                         'Top 24 NVDR Sell.json',
                                         json.dumps(nvdr_top_24_sell_lists),
                                         x_ms_blob_content_type='application/json')

    blobService.put_block_blob_from_text('radars-data',
                                         'Top 24 NVDR Sell.json',
                                         json.dumps({'desc': u'หุ้น 24 ตัวแรกที่ NVDR ขายเยอะที่สุด',
                                                     'header': ['Name', 'Sell'],
                                                     'data': json_table}, default=complex_python),
                                         x_ms_blob_content_type='application/json')


def gen_nvdr_top_24_value():
    cursor_local.execute("""SELECT symbol, datadate, volume_buy, volume_sell, volume_total, volume_net, volume_percent, value_buy, value_sell, value_total, value_net, value_percent
                            FROM stockgrid.nvdr_history
                            WHERE
                                datadate =
                                    (SELECT datadate FROM stockgrid.nvdr_history ORDER BY datadate desc LIMIT 1) and
                                symbol in (select symbol from stockgrid.datastock)
                            ORDER BY value_total desc LIMIT 24;
                         """)

    json_table = []
    nvdr_top_24_value_lists = []

    for symbol, datadate, volume_buy, volume_sell, volume_total, volume_net, volume_percent, value_buy, value_sell, value_total, value_net, value_percent, in cursor_local.fetchall():
        nvdr_top_24_value_lists.append(symbol)
        json_table.append({
            'Name': symbol,
            'Most Active': value_net
        })

    radars_count['NVDR'].append(
        {"count": len(nvdr_top_24_value_lists), "name": "Top 24 NVDR Most Active", "key": "Top 24 NVDR Most Active"})
    blobService.put_block_blob_from_text('radars-data',
                                         'Top 24 NVDR Most Active.json',
                                         json.dumps({'desc': u'หุ้น 24 ตัวแรกที่ NVDR มีมูลค่าซื้อขายเยอะที่สุด',
                                                     'header': ['Name', 'Most Active'],
                                                     'data': json_table}, default=complex_python),
                                         x_ms_blob_content_type='application/json')

    blobService.put_block_blob_from_text('radars',
                                         'Top 24 NVDR Most Active.json',
                                         json.dumps(nvdr_top_24_value_lists),
                                         x_ms_blob_content_type='application/json')


def gen_nvdr_top_24_ratio():
    cursor_local.execute("""SELECT symbol, datadate, volume_buy, volume_sell, volume_total, volume_net, volume_percent, value_buy, value_sell, value_total, value_net, value_percent
                            FROM stockgrid.nvdr_history
                            WHERE
                                datadate =
                                    (SELECT datadate FROM stockgrid.nvdr_history ORDER BY datadate desc LIMIT 1) and
                                symbol in (select symbol from stockgrid.datastock)
                            ORDER BY value_percent desc LIMIT 24;
                         """)

    json_table = []
    nvdr_top_24_ratio_lists = []

    for symbol, datadate, volume_buy, volume_sell, volume_total, volume_net, volume_percent, value_buy, value_sell, value_total, value_net, value_percent, in cursor_local.fetchall():
        nvdr_top_24_ratio_lists.append(symbol)
        json_table.append({
            'Name': symbol,
            'Ratio': value_percent
        })

    radars_count['NVDR'].append(
        {"count": len(nvdr_top_24_ratio_lists), "name": "Top 24 NVDR Ratio", "key": "Top 24 NVDR Ratio"})
    blobService.put_block_blob_from_text('radars-data',
                                         'Top 24 NVDR Ratio.json',
                                         json.dumps({
                                                        'desc': u'หุ้น 24 ตัวแรก ที่มีสัดส่วนนักลงทุนต่างชาติเข้ามาซื้อขายเยอะที่สุด โดยคิดจากมูลค่าซื้อขาย',
                                                        'header': ['Name', 'Ratio'],
                                                        'data': json_table}, default=complex_python),
                                         x_ms_blob_content_type='application/json')

    blobService.put_block_blob_from_text('radars',
                                         'Top 24 NVDR Ratio.json',
                                         json.dumps(nvdr_top_24_ratio_lists),
                                         x_ms_blob_content_type='application/json')


def gen_insider_report_buy():
    cursor_local.execute("""select distinct symbol
                            from
                                `stockgrid`.`insider_report`
                            where
                                filing_date = (SELECT oaq_date FROM stockgrid.oaq_date ORDER BY oaq_date DESC LIMIT 1) and
                                (
                                    `method` = 'Buy' or
                                    `method` = 'Receive'
                                )
                            group by symbol
                            order by sum(volume * price) desc""")

    insider_buy = [_symbol for _symbol, in cursor_local.fetchall()]

    radars_count['Insider Trading'] = [
        {
            'count': len(insider_buy),
            'name': 'Insider Buy',
            'key': 'Insider Buy'
        }]

    blobService.put_block_blob_from_text('radars',
                                         'Insider Buy.json',
                                         json.dumps(insider_buy),
                                         x_ms_blob_content_type='application/json')

    cursor_local.execute("""SELECT symbol, local_name, english_name, relationship, securities_type, filing_date, transaction_date, volume, price, method, remark
                            FROM stockgrid.insider_report
                            where
                                filing_date = (SELECT oaq_date FROM stockgrid.oaq_date ORDER BY oaq_date DESC LIMIT 1) and
                                (
                                    `method` = 'Buy' or
                                    `method` = 'Receive'
                                )
                            order by volume * price desc;""")

    json_table = []

    for symbol, local_name, english_name, relationship, securities_type, filing_date, transaction_date, volume, price, method, remark, in cursor_local.fetchall():
        json_table.append({
            'Name': symbol,
            'Name of Management': english_name if english_name else local_name,
            'Transaction Date': transaction_date,
            'Amount': volume,
            'Average Price(Baht)': price
        })

    blobService.put_block_blob_from_text('radars-data',
                                         'Insider Buy.json',
                                         json.dumps({'desc': u'รายชื่อหุ้นที่ผู้บริหารซื้อหรือรับโอน',
                                                     'header': ['Name', 'Name of Management', 'Transaction Date',
                                                                'Amount', 'Average Price(Baht)'],
                                                     'data': json_table}, default=complex_python),
                                         x_ms_blob_content_type='application/json')

    blobService.put_block_blob_from_text('radars-data',
                                         'Insider Trading.json',
                                         json.dumps({
                                             'desc': u'List of management who submitted the changes in securities holding report (Form 59-2) ',
                                             'header': [],
                                             'data': [
                                                 {'name': 'Insider Buy',
                                                  'desc': u'รายชื่อหุ้นที่ผู้บริหารซื้อหรือรับโอน'},
                                                 {'name': 'Insider Sell',
                                                  'desc': u'รายชื่อหุ้นที่ผู้บริหารขายหรือโอนออก'}]}),
                                         x_ms_blob_content_type='application/json')


def gen_insider_report_sell():
    cursor_local.execute("""select distinct symbol
                            from
                                `stockgrid`.`insider_report`
                            where
                                filing_date = (SELECT oaq_date FROM stockgrid.oaq_date ORDER BY oaq_date DESC LIMIT 1) and
                                (`method` = 'Sell' or `method` = 'Transfer')
                            group by symbol
                            order by sum(volume * price) desc
                                """)

    insider_sell = [_symbol for _symbol, in cursor_local.fetchall()]

    radars_count['Insider Trading'].append({
        'count': len(insider_sell),
        'name': 'Insider Sell',
        'key': 'Insider Sell'
    })

    blobService.put_block_blob_from_text('radars',
                                         'Insider Sell.json',
                                         json.dumps(insider_sell),
                                         x_ms_blob_content_type='application/json')

    cursor_local.execute("""SELECT symbol, local_name, english_name, relationship, securities_type, filing_date, transaction_date, volume, price, method, remark
                            FROM stockgrid.insider_report
                            where
                                filing_date = (SELECT oaq_date FROM stockgrid.oaq_date ORDER BY oaq_date DESC LIMIT 1) and
                                (
                                    `method` = 'Sell' or
                                    `method` = 'Transfer'
                                )
                            order by volume * price desc;""")

    json_table = []

    for symbol, local_name, english_name, relationship, securities_type, filing_date, transaction_date, volume, price, method, remark, in cursor_local.fetchall():
        json_table.append({
            'Name': symbol,
            'Name of Management': english_name if english_name else local_name,
            'Transaction Date': transaction_date,
            'Amount': volume,
            'Average Price(Baht)': price
        })

    blobService.put_block_blob_from_text('radars-data',
                                         'Insider Sell.json',
                                         json.dumps({'desc': u'รายชื่อหุ้นที่ผู้บริหารขายหรือโอนออก',
                                                     'header': ['Name', 'Name of Management', 'Transaction Date',
                                                                'Amount', 'Average Price(Baht)'],
                                                     'data': json_table}, default=complex_python),
                                         x_ms_blob_content_type='application/json')


def gen_radars_file():
    connect_data = pyodbc.connect(const.linuxthdata)
    cursor_data = connect_data.cursor()
    connect = pyodbc.connect(const.linuxthweb)
    cursor = connect.cursor()

    show_symbol = {}
    list_radars_in_service = defaultdict(list)

    indicator_list = {
        'performance': [],
        'overextended': [],
        'volume': [],
        'crossover': [],
        'radar101': [],
        'pesector': [],
        'candlepattern': [],
        'Chart Pattern': [],
        'kimeng': [],
        'ltf': [],
        'moneychannel': [],
        'stochastic': [],
        'radar2013': [],
        'Cash Balance Radars': [],
        'NVDR': [],
        'Insider Trading': []
    }

    table_type_show_name = {}

    cursor.execute("SELECT * FROM [stockgrid].services")
    services_rows = cursor.fetchall()

    for row in services_rows:
        if row.services_category <> 'volumeevent':
            table_type_show_name[row.services_category] = row.services_name
        else:
            table_type_show_name['volume'] = row.services_name

    cursor.execute("SELECT * FROM [stockgrid].tablecount ORDER BY table_order DESC")
    rows = cursor.fetchall()

    for row in rows:

        if row.table_type in table_type_show_name:
            table_type = table_type_show_name[row.table_type]
        else:
            table_type = row.table_type
            print table_type

        list_radars_in_service[table_type].append(
            {'name': row.table_showname, 'desc': '' if row.long_desc is None else row.long_desc})

        if row.short_desc:
            if row.table_type in indicator_list:
                indicator_list[row.table_type].append({
                    'name': row.table_showname,
                    'count': int(row.table_count),
                    'key': row.table_name
                })

            blobService.put_block_blob_from_text('radars-data',
                                                 row.table_showname + '.json',
                                                 json.dumps({
                                                     'desc': '' if row.long_desc is None else row.long_desc,
                                                     'header': [],
                                                     'data': []}),
                                                 x_ms_blob_content_type='application/json')
            continue

        cursor_data.execute("SELECT datadate, symbol "
                            "FROM {} "
                            "WHERE  datadate = (SELECT TOP 1 oaq_date FROM dbo.dateoaq ORDER BY oaq_date DESC)"
                            .format(row.table_name))

        radars_datas = cursor_data.fetchall()

        is_first = True
        count_radars = 0

        for radars_data in radars_datas:

            if radars_data.symbol in show_symbol:
                if table_type in show_symbol[radars_data.symbol]:
                    show_symbol[radars_data.symbol][table_type].append(row.table_showname)
                else:
                    show_symbol[radars_data.symbol][table_type] = [row.table_showname]
            else:
                show_symbol[radars_data.symbol] = {table_type: []}
                show_symbol[radars_data.symbol][table_type] = [row.table_showname]

            count_radars += 1

        cursor.commit()

        blobService.put_block_blob_from_text('radars-data',
                                             row.table_showname + '.json',
                                             json.dumps({
                                                 'desc': row.long_desc,
                                                 'header': [],
                                                 'data': []}),
                                             x_ms_blob_content_type='application/json')

        indicator_list[row.table_type].append({
            'name': row.table_showname,
            'count': count_radars,
            'key': row.table_name
        })

    for row in services_rows:
        blobService.put_block_blob_from_text('radars-data',
                                             row.services_name + '.json',
                                             json.dumps({
                                                 'desc': row.services_desc,
                                                 'header': [],
                                                 'data': list_radars_in_service[row.services_name]}),
                                             x_ms_blob_content_type='application/json')

    new_radars = {
        'Cash Balance Radars': ['Restricted', 'Cash Balance', 'Unrestricted'],
        'NVDR': ['Top 24 NVDR Buy', 'Top 24 NVDR Sell', 'Top 24 NVDR Most Active', 'Top 24 NVDR Ratio'],
        'Insider Trading': ['Insider Buy', 'Insider Sell']
    }

    for k in new_radars.keys():
        indicator_list[k] = []

        for i in new_radars[k]:
            print k, i
            radars_json = requests.get('http://stockradars.blob.core.windows.net/radars/{}.json'.format(i)).json()

            for j in radars_json:
                if j in show_symbol:
                    if k in show_symbol[j]:
                        show_symbol[j][k].append(i)
                    else:
                        show_symbol[j][k] = [i]
                else:
                    show_symbol[j] = {k: []}
                    show_symbol[j][k] = [i]

            indicator_list[k].append(
                {
                    'name': i,
                    'count': len(radars_json),
                    'key': i
                }
            )

    json.dump(show_symbol, open('show_symbol.json', 'w'))

    blobService.put_block_blob_from_path('stocksymbol',
                                         'show_symbol.json',
                                         'show_symbol.json',
                                         x_ms_blob_content_type='application/json')

    json.dump(indicator_list, open('radars_count.json', 'w'))
    blobService.put_block_blob_from_path('stocksymbol',
                                         'radars_count.json',
                                         'radars_count.json',
                                         x_ms_blob_content_type='application/json')

    print 'count finish'


__author__ = 'E425'

if __name__ == '__main__':
    try:
        holiday = const.SET_HOLIDAYS

        if not datetime.now().strftime("%d-%m-%Y") in holiday:
            ALL_SYMBOL = []
            cursor_local.execute("select symbol from `stockgrid`.`datastock`")

            ALL_SYMBOL = [_temp for _temp, in cursor_local.fetchall()]

            r_radars_count = requests.get(r'http://stockradars.blob.core.windows.net/stocksymbol/radars_count.json')
            radars_count = r_radars_count.json()

            gen_cash_balance_lists()
            gen_nvdr_top_24_buy()
            gen_nvdr_top_24_sell()
            gen_nvdr_top_24_value()
            gen_nvdr_top_24_ratio()

            gen_insider_report_buy()
            gen_insider_report_sell()

            blobService.put_block_blob_from_text('stocksymbol',
                                                 'radars_count.json',
                                                 json.dumps(radars_count),
                                                 x_ms_blob_content_type='application/json')

            gen_radars_file()
    except:
        e = traceback.format_exc()
        print e
        mail.send(e, '[SET] radars create radars from market')
