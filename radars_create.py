from cons import const
import datetime
import json
import mysql.connector
import pyodbc
import collections
import operator

__author__ = 'E425'


def ice_float(number):
    if not number is None:
        return float(number.replace(',', ''))
    return number

def is_24_or_not(list):
    if len(list) > 24:
        return 24;
    return len(list)

def top_24_lower_de():
    print("top_24_lower_de")
    sorted_x = sorted(temp_de.items(), key=operator.itemgetter(1))
    for num in range(0, is_24_or_not(sorted_x)):
        print('%d DE after sort %d : %f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        azure_cursor.execute("""
            INSERT INTO DAILY_RADARS (D_trade, I_security, I_radars, I_order)
            VALUES (?, ?, ?, ?)
            """, (datetime.date.today(), sorted_x[num][0], 193, num))



def top_24_current_ratio():
    print("top_24_current_ratio")
    sorted_x = sorted(temp_current.items(), key=operator.itemgetter(1), reverse=True)
    for num in range(0, is_24_or_not(sorted_x)):
        print('%d Current after sort %d : %f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        azure_cursor.execute("""
            INSERT INTO DAILY_RADARS (D_trade, I_security, I_radars, I_order)
            VALUES (?, ?, ?, ?)
            """, (datetime.date.today(), sorted_x[num][0], 194, num))


def top_24_roa_ratio():
    print("top_24_roa_ratio")
    sorted_x = sorted(temp_roa.items(), key=operator.itemgetter(1), reverse=True)
    for num in range(0, is_24_or_not(sorted_x)):
        print('%d ROA after sort %d : %f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        azure_cursor.execute("""
            INSERT INTO DAILY_RADARS (D_trade, I_security, I_radars, I_order)
            VALUES (?, ?, ?, ?)
            """, (datetime.date.today(), sorted_x[num][0], 195, num))


def top_24_roe_ratio():
    print("top_24_roe_ratio")
    sorted_x = sorted(temp_roe.items(), key=operator.itemgetter(1), reverse=True)
    for num in range(0, is_24_or_not(sorted_x)):
        print('%d ROE after sort %d : %f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        azure_cursor.execute("""
            INSERT INTO DAILY_RADARS (D_trade, I_security, I_radars, I_order)
            VALUES (?, ?, ?, ?)
            """, (datetime.date.today(), sorted_x[num][0], 183, num))


def top_24_npm_ratio():
    print("top_24_npm_ratio")
    sorted_x = sorted(temp_npm.items(), key=operator.itemgetter(1), reverse=True)
    for num in range(0, is_24_or_not(sorted_x)):
        print('%d NPM after sort %d : %f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        azure_cursor.execute("""
            INSERT INTO DAILY_RADARS (D_trade, I_security, I_radars, I_order)
            VALUES (?, ?, ?, ?)
            """, (datetime.date.today(), sorted_x[num][0], 178, num))


def top_24_gpm_ratio():
    print("top_24_gpm_ratio")
    sorted_x = sorted(temp_gpm.items(), key=operator.itemgetter(1), reverse=True)
    for num in range(0, is_24_or_not(sorted_x)):
        print('%d GPM after sort %d : %f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        azure_cursor.execute("""
            INSERT INTO DAILY_RADARS (D_trade, I_security, I_radars, I_order)
            VALUES (?, ?, ?, ?)
            """, (datetime.date.today(), sorted_x[num][0], 187, num))


def top_24_ebitda_ratio():
    print("top_24_ebitda_ratio")
    sorted_x = sorted(temp_ebitda.items(), key=operator.itemgetter(1), reverse=True)
    for num in range(0, is_24_or_not(sorted_x)):
        print('%d EBITDA after sort %d : %f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        azure_cursor.execute("""
            INSERT INTO DAILY_RADARS (D_trade, I_security, I_radars, I_order)
            VALUES (?, ?, ?, ?)
            """, (datetime.date.today(), sorted_x[num][0], 198, num))


def net_growth_every5y():
    print("net_growth_every5y")
    sorted_x = sorted(temp_5y_net_growth.items(), key=operator.itemgetter(1))
    for num in range(0, len(sorted_x)):
        print('%d Net Profit after sort %d : %f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        azure_cursor.execute("""
            INSERT INTO DAILY_RADARS (D_trade, I_security, I_radars, I_order)
            VALUES (?, ?, ?, ?)
            """, (datetime.date.today(), sorted_x[num][0], 177, num))


def net_positive_every5y():
    print("net_positive_every5y")
    sorted_x = sorted(temp_5y_net_positive.items(), key=operator.itemgetter(1))
    for num in range(0, len(sorted_x)):
        print('%d Net Profit Positive after sort %d : %f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        azure_cursor.execute("""
            INSERT INTO DAILY_RADARS (D_trade, I_security, I_radars, I_order)
            VALUES (?, ?, ?, ?)
            """, (datetime.date.today(), sorted_x[num][0], 182, num))


def net_growth15_every5y():
    print("net_positive_every5y")
    sorted_x = sorted(temp_5y_net_growth15.items(), key=operator.itemgetter(1))
    for num in range(0, len(sorted_x)):
        print('%d Net Profit 15 after sort %d : %f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        azure_cursor.execute("""
            INSERT INTO DAILY_RADARS (D_trade, I_security, I_radars, I_order)
            VALUES (?, ?, ?, ?)
            """, (datetime.date.today(), sorted_x[num][0], 180, num))


def roa_growth_every5y():
    print("ROA_growth_every5y")
    sorted_x = sorted(temp_5y_roa_growth.items(), key=operator.itemgetter(1))
    for num in range(0, len(sorted_x)):
        print('%d ROA Growth after sort %d : %f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        azure_cursor.execute("""
            INSERT INTO DAILY_RADARS (D_trade, I_security, I_radars, I_order)
            VALUES (?, ?, ?, ?)
            """, (datetime.date.today(), sorted_x[num][0], 196, num))


def roe_growth_every5y():
    print("ROE_growth_every5y")
    sorted_x = sorted(temp_5y_roe_growth.items(), key=operator.itemgetter(1))
    for num in range(0, len(sorted_x)):
        print('%d ROE Growth after sort %d : %f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        azure_cursor.execute("""
            INSERT INTO DAILY_RADARS (D_trade, I_security, I_radars, I_order)
            VALUES (?, ?, ?, ?)
            """, (datetime.date.today(), sorted_x[num][0], 184, num))


def npm_growth_sector_every5y():
    print("npm_growth_sector_every5y")
    sorted_x = sorted(temp_5y_npm_above_sector.items(), key=operator.itemgetter(1), reverse=True)
    for num in range(0, len(sorted_x)):
        print('%d NPM Growth Sector after sort %d : %f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        azure_cursor.execute("""
            INSERT INTO DAILY_RADARS (D_trade, I_security, I_radars, I_order)
            VALUES (?, ?, ?, ?)
            """, (datetime.date.today(), sorted_x[num][0], 181, num))


def rev_growth_sector_every5y():
    print("npm_growth_sector_every5y")
    sorted_x = sorted(temp_5y_rev_above_sector.items(), key=operator.itemgetter(1), reverse=True)
    for num in range(0, len(sorted_x)):
        print('%d Revenue Growth Sector after sort %d : %f' % (num ,sorted_x[num][0], sorted_x[num][1]))
        azure_cursor.execute("""
            INSERT INTO DAILY_RADARS (D_trade, I_security, I_radars, I_order)
            VALUES (?, ?, ?, ?)
            """, (datetime.date.today(), sorted_x[num][0], 197, num))


def NetGrow5Y(_security_id):
    if(_security_id in temp_year_0 and _security_id in temp_year_1 and _security_id in temp_year_2 and
                   _security_id in temp_year_3 and _security_id in temp_year_4 and _security_id in temp_year_5):
        #print("%f %f %f %f %f %f" % (temp_year_0[_security_id][2],temp_year_1[_security_id][2],temp_year_2[_security_id][2],temp_year_3[_security_id][2],temp_year_4[_security_id][2],temp_year_5[_security_id][2]))
        net_count = 0
        net_temp = 0
        #print(temp_year_0[_security_id][2])
        net_temp = temp_year_0[_security_id][2]

        if(net_temp <= temp_year_1[_security_id][2]):
            net_count += 1
            net_temp = temp_year_1[_security_id][2]
        elif(temp_year_1[_security_id][5] < 0):
            net_count -= 1

        if(net_temp <= temp_year_2[_security_id][2]):
            net_count += 1
            net_temp = temp_year_2[_security_id][2]
        elif(temp_year_1[_security_id][5] < 0):
            net_count -= 1

        if(net_temp <= temp_year_3[_security_id][2]):
            net_count += 1
            net_temp = temp_year_3[_security_id][2]
        elif(temp_year_1[_security_id][5] < 0):
            net_count -= 1

        if(net_temp <= temp_year_4[_security_id][2]):
            net_count += 1
            net_temp = temp_year_4[_security_id][2]
        elif(temp_year_1[_security_id][5] < 0):
            net_count -= 1

        if(net_temp <= temp_year_5[_security_id][2]):
            net_count += 1
            net_temp = temp_year_5[_security_id][2]
        elif(temp_year_1[_security_id][5] < 0):
            net_count -= 1

        if(net_count == 4 and temp_year_0[_security_id][5] < temp_year_5[_security_id][5] and
            temp_year_0[_security_id][2] < temp_year_5[_security_id][2]):
            #print("%d %f > %f > %f > %f > %f > %f" % ( _security_id,temp_year_0[_security_id][2],temp_year_1[_security_id][2],temp_year_2[_security_id][2],temp_year_3[_security_id][2],temp_year_4[_security_id][2],temp_year_5[_security_id][2]))
            return True
        if(net_count == 5):
            #print("%d %f > %f > %f > %f > %f > %f" % ( _security_id,temp_year_0[_security_id][2],temp_year_1[_security_id][2],temp_year_2[_security_id][2],temp_year_3[_security_id][2],temp_year_4[_security_id][2],temp_year_5[_security_id][2]))
            return True
    return False


def NetPositive5Y(_security_id):
    if(_security_id in temp_year_0 and _security_id in temp_year_1 and _security_id in temp_year_2 and
                   _security_id in temp_year_3 and _security_id in temp_year_4 and _security_id in temp_year_5):
        net_count = 0

        if(temp_year_0[_security_id][2] > 0):
            net_count += 1

        if(temp_year_1[_security_id][2] > 0):
            net_count += 1

        if(temp_year_2[_security_id][2] > 0):
            net_count += 1

        if(temp_year_3[_security_id][2] > 0):
            net_count += 1

        if(temp_year_4[_security_id][2] > 0):
            net_count += 1

        if(temp_year_5[_security_id][2] > 0):
            net_count += 1

        if(temp_year_1[_security_id][6] > 0 and temp_year_1[_security_id][6] > 0 and temp_year_2[_security_id][6] > 0
            and temp_year_3[_security_id][6] > 0 and temp_year_4[_security_id][6] > 0 and temp_year_5[_security_id][6] > 0):
                if(net_count >= 6):
                    #print("%d %f > %f > %f > %f > %f" % ( _security_id,temp_year_1[_security_id][2],temp_year_2[_security_id][2],temp_year_3[_security_id][2],temp_year_4[_security_id][2],temp_year_5[_security_id][2]))
                    return True

    return False


def NetGrow15_5Y(_security_id):
    if(_security_id in temp_year_0 and _security_id in temp_year_1 and _security_id in temp_year_2 and
                   _security_id in temp_year_3 and _security_id in temp_year_4 and _security_id in temp_year_5):
        #print("%f %f %f %f %f %f" % (temp_year_0[_security_id][2],temp_year_1[_security_id][2],temp_year_2[_security_id][2],temp_year_3[_security_id][2],temp_year_4[_security_id][2],temp_year_5[_security_id][2]))
        net_count = 0
        want_growth = 15
        x1 = 0
        x2 = 0
        x3 = 0
        x4 = 0
        x5 = 0

        if ((temp_year_1[_security_id][2]/temp_year_0[_security_id][2])-1) *100 > want_growth:
            net_count += 1
            x1 = ((temp_year_1[_security_id][2]/temp_year_0[_security_id][2])-1) *100

        if ((temp_year_2[_security_id][2]/temp_year_1[_security_id][2])-1) *100 > want_growth:
            net_count += 1
            x2 = ((temp_year_2[_security_id][2]/temp_year_1[_security_id][2])-1) *100

        if ((temp_year_3[_security_id][2]/temp_year_2[_security_id][2])-1) *100 > want_growth:
            net_count += 1
            x3 = ((temp_year_3[_security_id][2]/temp_year_2[_security_id][2])-1) *100

        if ((temp_year_4[_security_id][2]/temp_year_3[_security_id][2])-1) *100 > want_growth:
            net_count += 1
            x4 = ((temp_year_4[_security_id][2]/temp_year_3[_security_id][2])-1) *100

        if ((temp_year_5[_security_id][2]/temp_year_4[_security_id][2])-1) *100 > want_growth:
            net_count += 1
            x5 = ((temp_year_5[_security_id][2]/temp_year_4[_security_id][2])-1) *100

        if(net_count >= 4):
            avg = (x1 + x2 + x3 + x4 + x5) / 5
            #print("%d %d = %f %f %f %f %f" % (_security_id, avg, x1, x2, x3, x4, x5))
            return True
    return False


def RoaGrow5Y(_security_id):
    if(_security_id in temp_year_0 and _security_id in temp_year_1 and _security_id in temp_year_2 and
                   _security_id in temp_year_3 and _security_id in temp_year_4 and _security_id in temp_year_5):
        #print("%f %f %f %f %f %f" % (temp_year_0[_security_id][2],temp_year_1[_security_id][2],temp_year_2[_security_id][2],temp_year_3[_security_id][2],temp_year_4[_security_id][2],temp_year_5[_security_id][2]))
        net_count = 0
        net_temp = 100 * (temp_year_1[_security_id][2] / temp_year_1[_security_id][3])
        roa_1 = 100 * (temp_year_1[_security_id][2] / temp_year_1[_security_id][3])
        roa_2 = 100 * (temp_year_2[_security_id][2] / temp_year_2[_security_id][3])
        roa_3 = 100 * (temp_year_3[_security_id][2] / temp_year_3[_security_id][3])
        roa_4 = 100 * (temp_year_4[_security_id][2] / temp_year_4[_security_id][3])
        roa_5 = 100 * (temp_year_5[_security_id][2] / temp_year_5[_security_id][3])

        if(roa_1 > 0):
            net_count += 1

        if(net_temp <= roa_2 and roa_2 > 0):
            net_count += 1
            net_temp = roa_2

        if(net_temp <= roa_3 and roa_3 > 0):
            net_count += 1
            net_temp = roa_3

        if(net_temp <= roa_4 and roa_4 > 0):
            net_count += 1
            net_temp = roa_4

        if(net_temp <= roa_5 and roa_5 > 0):
            net_count += 1

        if(net_count >= 4 and roa_1 < roa_5):
            #print("%d xx %f > %f > %f > %f > %f" % ( _security_id,roa_1,roa_2,roa_3,roa_4,roa_5 ))
            return True
    return False


def RoeGrow5Y(_security_id):
    if(_security_id in temp_year_0 and _security_id in temp_year_1 and _security_id in temp_year_2 and
                   _security_id in temp_year_3 and _security_id in temp_year_4 and _security_id in temp_year_5):
        #print("%f %f %f %f %f %f" % (temp_year_0[_security_id][2],temp_year_1[_security_id][2],temp_year_2[_security_id][2],temp_year_3[_security_id][2],temp_year_4[_security_id][2],temp_year_5[_security_id][2]))
        net_count = 0
        net_temp = 100 * (temp_year_1[_security_id][2] / temp_year_1[_security_id][4])
        roe_1 = 100 * (temp_year_1[_security_id][2] / temp_year_1[_security_id][4])
        roe_2 = 100 * (temp_year_2[_security_id][2] / temp_year_2[_security_id][4])
        roe_3 = 100 * (temp_year_3[_security_id][2] / temp_year_3[_security_id][4])
        roe_4 = 100 * (temp_year_4[_security_id][2] / temp_year_4[_security_id][4])
        roe_5 = 100 * (temp_year_5[_security_id][2] / temp_year_5[_security_id][4])

        if(roe_1 > 0):
            net_count += 1

        if(net_temp <= roe_2 and roe_2 > 0):
            net_count += 1
            net_temp = roe_2

        if(net_temp <= roe_3 and roe_3 > 0):
            net_count += 1
            net_temp = roe_3

        if(net_temp <= roe_4 and roe_4 > 0):
            net_count += 1
            net_temp = roe_4

        if(net_temp <= roe_5 and roe_5 > 0):
            net_count += 1

        if(net_count >= 4 and roe_1 < roe_5):
            #print("%d roe %f > %f > %f > %f > %f" % ( _security_id,roe_1,roe_2,roe_3,roe_4,roe_5 ))
            return True
    return False


def NPM5YThenAvg(_security_id):
    if(_security_id in temp_year_0 and _security_id in temp_year_1 and _security_id in temp_year_2 and
                   _security_id in temp_year_3 and _security_id in temp_year_4 and _security_id in temp_year_5):
        #print("%f %f %f %f %f %f" % (temp_year_0[_security_id][2],temp_year_1[_security_id][2],temp_year_2[_security_id][2],temp_year_3[_security_id][2],temp_year_4[_security_id][2],temp_year_5[_security_id][2]))
        npm_1 = 100 * (temp_year_1[_security_id][2] / temp_year_1[_security_id][5])
        npm_2 = 100 * (temp_year_2[_security_id][2] / temp_year_2[_security_id][5])
        npm_3 = 100 * (temp_year_3[_security_id][2] / temp_year_3[_security_id][5])
        npm_4 = 100 * (temp_year_4[_security_id][2] / temp_year_4[_security_id][5])
        npm_5 = 100 * (temp_year_5[_security_id][2] / temp_year_5[_security_id][5])

        avg = (npm_1 + npm_2 + npm_3 + npm_4 + npm_5) / 5

        #print("%d npm %f > %f" % ( _security_id,avg, temp_sector_npm[temp_year_3[_security_id][7]]))
        if(avg > temp_sector_npm[temp_year_3[_security_id][7]] and npm_1 > 0 and npm_2 > 0 and npm_3 > 0 and npm_4 > 0 and npm_5 > 0):
            #print("%d npm %f > %f" % ( _security_id,avg, temp_sector_npm[temp_year_3[_security_id][7]]))
            return avg
    return 0


def Revenue5YThenAvg(_security_id):
    if(_security_id in temp_year_0 and _security_id in temp_year_1 and _security_id in temp_year_2 and
                   _security_id in temp_year_3 and _security_id in temp_year_4 and _security_id in temp_year_5):
        #print("%f %f %f %f %f %f" % (temp_year_0[_security_id][2],temp_year_1[_security_id][2],temp_year_2[_security_id][2],temp_year_3[_security_id][2],temp_year_4[_security_id][2],temp_year_5[_security_id][2]))
        rev_1 = temp_year_1[_security_id][5]
        rev_2 = temp_year_2[_security_id][5]
        rev_3 = temp_year_3[_security_id][5]
        rev_4 = temp_year_4[_security_id][5]
        rev_5 = temp_year_5[_security_id][5]

        avg = (rev_1 + rev_2 + rev_3 + rev_4 + rev_5) / 5

        #print("%d npm %f > %f" % ( _security_id,avg, temp_sector_npm[temp_year_3[_security_id][7]]))
        if(avg > temp_sector_revenue[temp_year_3[_security_id][7]] and rev_1 > 0 and rev_2 > 0 and rev_3 > 0 and rev_4 > 0 and rev_5 > 0):
            #print("%d rev %f > %f" % ( _security_id,avg, temp_sector_revenue[temp_year_3[_security_id][7]]))
            return avg
    return 0


def main():
    pass


if __name__ == '__main__':


    mysql_db = mysql.connector.connect(**const.ocean_mysql)
    sql_db = pyodbc.connect(const.windowsthweb_test)
    azure_db = pyodbc.connect(const.windowsthdata_test)

    mysql_cursor = mysql_db.cursor()
    sql_cursor = sql_db.cursor()
    azure_cursor = azure_db.cursor()

    mini_db = []
    mini_avg_db = []
    security_ids = set()
    sector_ids = set()

    temp_price = {}
    temp_de = {}
    temp_current = {}
    temp_roa = {}
    temp_roe = {}
    temp_npm = {}
    temp_gpm = {}
    temp_ebitda = {}

    temp_stock = {}

    temp_sector_de = {}
    temp_sector_de_count = {}

    temp_sector_current = {}
    temp_sector_current_count = {}

    temp_sector_roa = {}
    temp_sector_roa_count = {}

    temp_sector_roe = {}
    temp_sector_roe_count = {}

    temp_sector_npm = {}
    temp_sector_npm_count = {}

    temp_sector_gpm = {}
    temp_sector_gpm_count = {}

    temp_sector_ebitda = {}
    temp_sector_ebitda_count = {}

    temp_sector_revenue = {}
    temp_sector_revenue_count = {}

    print("XXX")

    sql_cursor.execute("""SELECT [I_SECTOR] ,Count([I_SECTOR])
                        FROM [psims].[COMPANY_FIN]
                        WHERE I_FS_TYPE = 'C'
                        AND I_YEAR = 2012
                        AND I_QUARTER = 9
                        group by [I_SECTOR]""")

    for index, (_sector, count) in enumerate(sql_cursor):
        #DE
        temp_sector_de_count[_sector] = 0
        temp_sector_de[_sector] = 0
        #Current
        temp_sector_current_count[_sector] = 0
        temp_sector_current[_sector] = 0
        #ROA
        temp_sector_roa_count[_sector] = 0
        temp_sector_roa[_sector] = 0
        #ROE
        temp_sector_roe_count[_sector] = 0
        temp_sector_roe[_sector] = 0
        #NPM
        temp_sector_npm_count[_sector] = 0
        temp_sector_npm[_sector] = 0
        #GPM
        temp_sector_gpm_count[_sector] = 0
        temp_sector_gpm[_sector] = 0
        #EBITDA / SALE
        temp_sector_ebitda_count[_sector] = 0
        temp_sector_ebitda[_sector] = 0
        #Revenue
        temp_sector_revenue_count[_sector] = 0
        temp_sector_revenue[_sector] = 0


        sector_ids.add(_sector)
        #print("%d %d" % (_sector,temp_sector_count[_sector]))

    sql_cursor.execute("SELECT I_SECURITY "
                       ",I_INDUSTRY"
                       ",I_SECTOR"
                       ",M_CURRENT_LIABILITY"
                       ",M_TOTAL_LIABILITY"
                       ",M_SHLD_EQUITY"
                       ",M_CURRENT_ASSET"
                       ",M_TOTAL_ASSET"
                       ",M_NET_PROFIT"
                       ",M_TOTAL_REVENUE"
                       ",M_NET_OPERATING"
                       ",M_COS"
                       ",R_EPS"
                       ",M_EBIT"
                       ",M_DP "
                       "FROM [psims].[COMPANY_FIN] "
                       "WHERE I_FS_TYPE = 'C' "
                       "AND I_YEAR = 2012 "
                       "AND I_QUARTER = 9 ")

    print("YYY")

    for index, (_security_id, _industry, _sector, _current_liability, _total_liability,
                _shld_equity, _current_asset,_total_asset,_net_profit, _total_revenue,
                _net_operating, _cos, _eps, _ebit, _dp) in enumerate(sql_cursor):

        security_ids.add(_security_id)
        temp_stock[_security_id] = [_security_id, _industry, _sector, _current_liability, _total_liability, _shld_equity, _current_asset,
            _total_asset,_net_profit, _total_revenue, _net_operating, _cos, _eps, _ebit, _dp]
        #print(temp_stock[_security_id])
        if _total_liability is not None and _shld_equity is not None:
            if (_total_liability / _shld_equity) <= 1 and (_total_liability / _shld_equity) > 0:
                temp_sector_de_count[_sector] = temp_sector_de_count[_sector] + 1
                temp_sector_de[_sector] = temp_sector_de[_sector]   + (_total_liability / _shld_equity)
                #print("DE Sector %d Count %d " % (_sector, temp_sector_de_count[_sector]))
        if _current_asset is not None and _current_liability is not None:
            if (_current_asset / _current_liability) >= 1:
                temp_sector_current_count[_sector] = temp_sector_current_count[_sector] + 1
                temp_sector_current[_sector] = temp_sector_current[_sector]   + (_current_asset / _current_liability)
                #print("Current Sector %d Count %d " % (_sector, temp_sector_current_count[_sector]))
        if _net_profit is not None and _total_asset is not None and _net_profit > 0:
            temp_sector_roa_count[_sector] = temp_sector_roa_count[_sector] + 1
            temp_sector_roa[_sector] = temp_sector_roa[_sector] + (100 * (_net_profit / _total_asset))
            #print("ROA Sector %d Count %d " % (_sector, temp_sector_roa_count[_sector]))
        if _net_profit is not None and _shld_equity is not None and _net_profit > 0:
            temp_sector_roe_count[_sector] = temp_sector_roe_count[_sector] + 1
            temp_sector_roe[_sector] = temp_sector_roe[_sector] + (100 * (_net_profit / _shld_equity))
            #print("ROE Sector %d Count %d " % (_sector, temp_sector_roe_count[_sector]))
        if _net_profit is not None and _total_revenue is not None and _net_profit > 0:
            temp_sector_npm_count[_sector] = temp_sector_npm_count[_sector] + 1
            temp_sector_npm[_sector] = temp_sector_npm[_sector] + (100 * (_net_profit / _total_revenue))
            #print("NPM Sector %d Count %d " % (_sector, temp_sector_npm_count[_sector]))
        if _cos is not None and _total_revenue is not None and _total_revenue > 0:
            temp_sector_gpm_count[_sector] = temp_sector_gpm_count[_sector] + 1
            temp_sector_gpm[_sector] = temp_sector_gpm[_sector] + (100 * ((_total_revenue - _cos) / _total_revenue))
            #print("GPM Sector %d Count %d " % (_sector, temp_sector_gpm_count[_sector]))
        if _ebit is not None and _total_revenue is not None and _ebit > 0 and _dp is not None:
            temp_sector_ebitda_count[_sector] = temp_sector_ebitda_count[_sector] + 1
            temp_sector_ebitda[_sector] = temp_sector_ebitda[_sector] + (100 * ((_ebit + _dp) / _total_revenue))
            #print("EBITDA Sector %d Count %d " % (_sector, temp_sector_ebitda_count[_sector]))
        if _total_revenue is not None:
            temp_sector_revenue_count[_sector] = temp_sector_revenue_count[_sector] + 1
            temp_sector_revenue[_sector] = temp_sector_revenue[_sector] + _total_revenue


    #Calculate Average by Sector
    for _sector in sector_ids:
        #DE
        if( temp_sector_de[_sector] != 0 and temp_sector_de_count[_sector] != 0):
            temp_sector_de[_sector] = temp_sector_de[_sector] / temp_sector_de_count[_sector]
        #print("Avg DE Sector %f" % (temp_sector_de[_sector]))
        #Current
        if( temp_sector_current[_sector] != 0 and temp_sector_current_count[_sector] != 0):
            temp_sector_current[_sector] = temp_sector_current[_sector] / temp_sector_current_count[_sector]
        #print("Avg Current Sector %f" % (temp_sector_current[_sector]))
        #ROA
        if( temp_sector_roa[_sector] != 0 and temp_sector_roa_count[_sector] != 0):
            temp_sector_roa[_sector] = temp_sector_roa[_sector] / temp_sector_roa_count[_sector]
        #print("Avg ROA Sector %f" % (temp_sector_roa[_sector]))
        #ROE
        if( temp_sector_roe[_sector] != 0 and temp_sector_roe_count[_sector] != 0):
            temp_sector_roe[_sector] = temp_sector_roe[_sector] / temp_sector_roe_count[_sector]
        #print("Avg ROE Sector %f" % (temp_sector_roe[_sector]))
        #NPM
        if( temp_sector_npm[_sector] != 0 and temp_sector_npm_count[_sector] != 0):
            temp_sector_npm[_sector] = temp_sector_npm[_sector] / temp_sector_npm_count[_sector]
        #print("Avg NPM Sector %f" % (temp_sector_npm[_sector]))
        #GPM
        if( temp_sector_gpm[_sector] != 0 and temp_sector_gpm_count[_sector] != 0):
            temp_sector_gpm[_sector] = temp_sector_gpm[_sector] / temp_sector_gpm_count[_sector]
        #print("Avg GPM Sector %f" % (temp_sector_gpm[_sector]))
        #EBITDA
        if( temp_sector_ebitda[_sector] != 0 and temp_sector_ebitda_count[_sector] != 0):
            temp_sector_ebitda[_sector] = temp_sector_ebitda[_sector] / temp_sector_ebitda_count[_sector]
        #print("Avg GPM Sector %f" % (temp_sector_ebitda[_sector]))
        #Revenue
        if( temp_sector_revenue[_sector] != 0 and temp_sector_revenue_count[_sector] != 0):
            temp_sector_revenue[_sector] = temp_sector_revenue[_sector] / temp_sector_revenue_count[_sector]


    #Calculate above Sector
    for _security_id in security_ids:
        #DE
        if temp_stock[_security_id][4] is not None and temp_stock[_security_id][5] is not None:
            temp = temp_stock[_security_id][4] / temp_stock[_security_id][5]
            if(temp < temp_sector_de[temp_stock[_security_id][2]] and temp > 0):
                temp_de[_security_id] = temp
        #Current
        if temp_stock[_security_id][6] is not None and temp_stock[_security_id][3] is not None:
            temp = temp_stock[_security_id][6] / temp_stock[_security_id][3]
            if(temp > temp_sector_current[temp_stock[_security_id][2]] and temp >= 1):
                temp_current[_security_id] = temp
                #print("Current %f" % (temp_current[_security_id]))
        #ROA
        if temp_stock[_security_id][8] is not None and temp_stock[_security_id][7] is not None and temp_stock[_security_id][8] > 0:
            temp = 100 * (temp_stock[_security_id][8] / temp_stock[_security_id][7])
            if(temp > temp_sector_roa[temp_stock[_security_id][2]]):
                temp_roa[_security_id] = temp
        #ROE
        if temp_stock[_security_id][8] is not None and temp_stock[_security_id][5] is not None and temp_stock[_security_id][8] > 0:
            temp = 100 * (temp_stock[_security_id][8] / temp_stock[_security_id][5])
            if(temp > temp_sector_roe[temp_stock[_security_id][2]]):
                temp_roe[_security_id] = temp
        #NPM
        if temp_stock[_security_id][8] is not None and temp_stock[_security_id][9] is not None and temp_stock[_security_id][8] > 0:
            temp = 100 * (temp_stock[_security_id][8] / temp_stock[_security_id][9])
            if(temp > temp_sector_npm[temp_stock[_security_id][2]]):
                temp_npm[_security_id] = temp
        #GPM
        if temp_stock[_security_id][11] is not None and temp_stock[_security_id][9] is not None and temp_stock[_security_id][9] > 0:
            temp = 100 * ((temp_stock[_security_id][9] - temp_stock[_security_id][11]) / temp_stock[_security_id][9])
            if(temp > temp_sector_gpm[temp_stock[_security_id][2]]):
                temp_gpm[_security_id] = temp
        #EBITDA Margin
        if temp_stock[_security_id][13] is not None and temp_stock[_security_id][9] is not None and temp_stock[_security_id][14] is not None:
            temp = 100 * ((temp_stock[_security_id][13] + temp_stock[_security_id][14]) / temp_stock[_security_id][9])
            if(temp > temp_sector_ebitda[temp_stock[_security_id][2]]):
                temp_ebitda[_security_id] = temp


    top_24_lower_de()
    top_24_current_ratio()
    top_24_roa_ratio()
    top_24_roe_ratio()
    top_24_npm_ratio()
    top_24_gpm_ratio()
    top_24_ebitda_ratio()


    startyear = 2006
    endyear = 2012
    #old year
    temp_year_0 = {}
    temp_year_1 = {}
    temp_year_2 = {}
    temp_year_3 = {}
    temp_year_4 = {}
    temp_year_5 = {}
    #last year

    temp_5y_net_growth = {}
    temp_5y_net_positive = {}
    temp_5y_net_growth15 = {}
    temp_5y_roa_growth = {}
    temp_5y_roe_growth = {}
    temp_5y_npm_above_sector ={}
    temp_5y_rev_above_sector ={}


    sql_cursor.execute("SELECT I_SECURITY "
                       ",I_SECTOR"
                       ",I_YEAR"
                       ",M_NET_PROFIT"
                       ",M_TOTAL_ASSET"
                       ",M_SHLD_EQUITY"
                       ",M_TOTAL_REVENUE"
                       ",M_NET_OPERATING"
                       " FROM [psims].[COMPANY_FIN] "
                       "WHERE I_FS_TYPE = 'C' "
                       "AND I_YEAR <= " + str(endyear) +
                       "AND I_YEAR > " + str(startyear) +
                       "AND I_QUARTER = 9 ")

    for index, (_security_id, _sector, _year, _net_profit, _total_asset, _shld_equity, _total_revenue, _net_operating) in enumerate(sql_cursor):
        #print("%d Year %d %f %f %f %f" % ( _security_id,_year, _net_profit, _total_asset, _shld_equity, _total_revenue))
        if(endyear - _year) == 0:
            #print("%d Year %d %f %f %f %f" % ( _security_id,_year, _net_profit, _total_asset, _shld_equity, _total_revenue))
            temp_year_5[_security_id] = [_security_id,_year, _net_profit, _total_asset, _shld_equity, _total_revenue, _net_operating, _sector]
            #print("%d Year %d %f %f" % (temp_year_5[_security_id][0], temp_year_5[_security_id][1], temp_year_5[_security_id][2], temp_year_5[_security_id][3]))
        if(endyear - _year) == 1:
            temp_year_4[_security_id] = [_security_id,_year, _net_profit, _total_asset, _shld_equity, _total_revenue, _net_operating, _sector]
        if(endyear - _year) == 2:
            temp_year_3[_security_id] = [_security_id,_year, _net_profit, _total_asset, _shld_equity, _total_revenue, _net_operating, _sector]
        if(endyear - _year) == 3:
            temp_year_2[_security_id] = [_security_id,_year, _net_profit, _total_asset, _shld_equity, _total_revenue, _net_operating, _sector]
        if(endyear - _year) == 4:
            temp_year_1[_security_id] = [_security_id,_year, _net_profit, _total_asset, _shld_equity, _total_revenue, _net_operating, _sector]
        if(endyear - _year) == 5:
            temp_year_0[_security_id] = [_security_id,_year, _net_profit, _total_asset, _shld_equity, _total_revenue, _net_operating, _sector]

    net_growth_count = 0
    net_positive_count = 0
    net_growth15_count = 0
    roa_growth_count = 0
    roe_growth_count = 0
    npm_growth5_count = 0
    rev_growth5_count = 0

    for _security_id in security_ids:
        if(NetGrow5Y(_security_id)):
            temp_5y_net_growth[_security_id] = net_growth_count
            net_growth_count += 1
        if(NetPositive5Y(_security_id)):
            temp_5y_net_positive[_security_id] = net_positive_count
            net_positive_count += 1
        if(NetGrow15_5Y(_security_id)):
            temp_5y_net_growth15[_security_id] = net_growth15_count
            net_growth15_count += 1
        if(RoaGrow5Y(_security_id)):
            temp_5y_roa_growth[_security_id] = roa_growth_count
            roa_growth_count += 1
        if(RoeGrow5Y(_security_id)):
            temp_5y_roe_growth[_security_id] = roe_growth_count
            roe_growth_count += 1
        temp_result = NPM5YThenAvg(_security_id)
        if(temp_result):
            temp_5y_npm_above_sector[_security_id] = temp_result
            npm_growth5_count += 1
        temp_result = Revenue5YThenAvg(_security_id)
        if(temp_result):
            temp_5y_rev_above_sector[_security_id] = temp_result
            rev_growth5_count += 1


    print("NetGrowthCount %d" % (net_growth_count))
    print("NetPositiveCount %d" % (net_positive_count))
    print("Net15Count %d" % (net_growth15_count))
    print("ROAGrowthCount %d" % (roa_growth_count))
    print("ROEGrowthCount %d" % (roe_growth_count))
    print("NPMGrowthCount %d" % (npm_growth5_count))
    print("REVGrowthCount %d" % (rev_growth5_count))

    net_growth_every5y()
    net_positive_every5y()
    net_growth15_every5y()
    roa_growth_every5y()
    roe_growth_every5y()
    npm_growth_sector_every5y()
    rev_growth_sector_every5y()


    print("Finish Programme")